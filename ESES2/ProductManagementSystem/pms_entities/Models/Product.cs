﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ProductManagementSystem.Entities.Models
{
    public class Product : IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string EanNumber { get; set; }
        public Brand Brand { get; set; }
        public List<Category> Categories { get; set; }
        public decimal Price { get; set; }
        public string ImageLocation { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public DateTime TimeCreated { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public bool Deleted { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public DateTime TimeDeleted { get; set; }
    }
}
